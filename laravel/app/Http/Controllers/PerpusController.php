<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PerpusController extends Controller
{
        public function lihatbuku()
    {
        // Panggil stored procedure lihatbuku dari database
        $perpus = DB::select('CALL lihatbuku()');

        return view('buku.lihatbuku', ['perpus' => $perpus]);
    }
    public function editbuku()
    {

        return view('buku.editbuku');
        
    }
    public function editbukuparam($kode_eksemplar)
    {   
        
        $perpus = DB::table('buku')
            ->where('kode_eksemplar', '=', $kode_eksemplar)
            ->select('kode_eksemplar')
            ->first(); // Menggunakan first() karena kita mengharapkan satu hasil saja

        return view('buku.editbuku', ['perpus' => $perpus, 'kode_eksemplar' => $kode_eksemplar]);
    }


    public function hapusbuku()
    {

        return view('buku.hapusbuku');
        
    }

    public function hapusbukuparam($kode_eksemplar)
    {   
        
        $perpus = DB::table('buku')
        ->where('kode_eksemplar', '=', $kode_eksemplar)
        ->select('kode_eksemplar')
        ->first();

        return view('buku.hapusbuku', ['perpus' => $perpus, 'kode_eksemplar' => $kode_eksemplar]);
    }

    public function peminjaman()
    {
        $peminjamandata = DB::select("CALL TampilTransaksi()");
        return view('peminjaman.lihatpeminjaman', ['peminjamandata' => $peminjamandata]);
    }
    
    public function lihatanggota()
    {
        $anggotaData = DB::table('anggota')
            ->select('nik', 'nama', 'email', 'alamat', 'telepon', 'jenis_kelamin')
            ->get();

        return view('anggota.lihatanggota', ['anggotaData' => $anggotaData]);
    }
    
    public function editanggota()
    {
        return view('anggota.editanggota');
    }
    public function editanggotaparam($nik)
    {
        $anggotaData = DB::table('anggota')
            ->select('nik', 'nama', 'email', 'alamat', 'telepon', 'jenis_kelamin')
            ->where('nik', '=', $nik)
            ->first();

        return view('anggota.editanggota', ['anggotaData' => $anggotaData,'nik' => $nik]);
    }
    public function hapusanggota()
    {
        return view('anggota.hapusanggota');
    }
    public function hapusanggotaparam($nik)
    {
        $anggotaData = DB::table('anggota')
            ->select('nik', 'nama', 'email', 'alamat', 'telepon', 'jenis_kelamin')
            ->where('nik', '=', $nik)
            ->first();

        return view('anggota.hapusanggota', ['anggotaData' => $anggotaData,'nik' => $nik]);
    }
    public function insertbuku(Request $request)
    {
        // Ambil data dari form
        $kode_eksemplar = $request->input('kode_eksemplar');
        $penerbit = $request->input('penerbit');
        $judul = $request->input('judul');
        $jumlah = $request->input('jumlah');
        $pengarang = $request->input('pengarang');

        // Panggil stored procedure InsertBuku dari database
        DB::select('CALL InsertBuku(?, ?, ?, ?, ?)', [$kode_eksemplar, $penerbit, $judul, $jumlah,$pengarang]);

        // Redirect ke lihatbuku dengan pesan sukses
        return redirect('buku')->with('status', 'Buku berhasil ditambahkan');
    }
    public function updatebuku(Request $request)
    {
        // Ambil data dari form
        $kode_eksemplar = $request->input('kode_eksemplar');
        $penerbit = $request->input('penerbit');
        $judul = $request->input('judul');
        $jumlah = $request->input('jumlah');
        $pengarang = $request->input('pengarang');

        // Panggil stored procedure InsertBuku dari database
        DB::select('CALL updatebuku(?, ?, ?, ?, ?)', [$kode_eksemplar, $penerbit, $judul, $jumlah,$pengarang]);

        // Redirect ke lihatbuku dengan pesan sukses
        return redirect('buku')->with('status', 'Buku berhasil diubah');
    }

    public function deletebuku(Request $request){
        $kode_eksemplar = $request->input('kode_eksemplar');
        DB::select('CALL deletebuku(?)', [$kode_eksemplar]);
        return redirect('buku')->with('status', 'Buku berhasil dihapus');
    }
    public function insertanggota(Request $request)
    {
        // Ambil data dari form
        $NIK = $request->input('NIK');
        $nama = $request->input('nama');
        $email = $request->input('email');
        $alamat = $request->input('alamat');
        $telepon = $request->input('telepon');
        $jenisKelamin = $request->input('jenisKelamin');

        // Panggil stored procedure InsertBuku dari database
        DB::select('CALL InsertAnggota(?, ?, ?, ?, ?, ?)', [$NIK, $nama, $email, $alamat, $telepon, $jenisKelamin]);
        return redirect('anggota')->with('status', 'Data berhasil ditambahkan');
    }
    public function updateanggota(Request $request)
    {
        // Ambil data dari form
        $NIK = $request->input('NIK');
        $nama = $request->input('nama');
        $email = $request->input('email');
        $alamat = $request->input('alamat');
        $telepon = $request->input('telepon');
        $jenisKelamin = $request->input('jenisKelamin');

        // Panggil stored procedure InsertBuku dari database
        DB::select('CALL updateanggota(?, ?, ?, ?, ?, ?)', [$NIK, $nama, $email, $alamat, $telepon, $jenisKelamin]);
        return redirect('anggota')->with('status', 'Data berhasil dirubah');
    }
    public function deleteanggota(Request $request)
    {
        $NIK = $request->input('NIK');
        DB::select('CALL deleteanggota(?)', [$NIK]);
        return redirect('anggota')->with('status', 'Buku berhasil dihapus');
    }



    public function tambahpeminjaman()
    {
        return view('peminjaman.tambahpeminjaman');
    }
    public function editpeminjaman()
    {
        return view('peminjaman.editpeminjaman');
    }
    public function hapuspeminjaman()
    {
        return view('peminjaman.hapuspeminjaman');
    }

    public function tambahprocess(Request $request)
    {
        // Validate the form data
        $request->validate([
            'NIK' => 'required|numeric',
            'angka' => 'required|numeric',
            'waktu_pinjam' => 'required|date',
            'kode_eksemplar.*' => 'required', // You may need to adjust this validation rule
        ]);

        // Retrieve form data
        $NIK = $request->input('NIK');
        $angka = $request->input('angka');
        $waktu_pinjam = $request->input('waktu_pinjam');
        $kode_eksemplar = $request->input('kode_eksemplar');

        $book_copy_codes = implode(',', $kode_eksemplar);

        $result = DB::select('CALL addtransaksi(?, ?, ?)', [$NIK, $book_copy_codes, $waktu_pinjam]);

        return redirect('peminjaman')->with('status', 'data berhasil ditambahkan');
        
    }
    public function hapusprocess(Request $request)
    {
        // Validate the form data
        $request->validate([
            'id_transaksi' => 'required|numeric'
        ]);
        // Retrieve form data
        $id_transaksi = $request->input('id_transaksi');
        $result = DB::select("CALL addtransaksi('$id_transaksi')");
        return redirect('peminjaman')->with('status', 'data berhasil dihapus');
        
    }
    public function hapuspeminjamanparam($id_transaksi)
    {
        $anggotaData = DB::table('transaksi')
            ->select('id_transaksi')
            ->where('id_transaksi', '=', $id_transaksi)
            ->first();

        return view('peminjaman.hapuspeminjaman', ['id_transaksi' => $id_transaksi]);
    }
    public function pengembalian()
    {
        $peminjamandata = DB::select("CALL TampilTransaksi()");
        return view('pengembalian.lihatpengembalian', ['peminjamandata' => $peminjamandata]);
    }
    public function pengembalianparam($id_transaksi)
    {
        $anggotaData = DB::table('transaksi')
            ->select('id_transaksi')
            ->where('id_transaksi', '=', $id_transaksi)
            ->first();
        return view('pengembalian.kembali', ['id_transaksi' => $id_transaksi]);
    }
    public function pengembalianprocess(Request $request)
    {
        $request->validate([
            'id_transaksi' => 'required|numeric',
            'waktu_kembali' => 'required|date'
        ]);
        // Retrieve form data
        $id_transaksi = $request->input('id_transaksi');
        $waktu_kembali = $request->input('waktu_kembali');
        $result = DB::select("CALL kembalikan_buku('$id_transaksi','$waktu_kembali')");
        return redirect('peminjaman')->with('status', 'data berhasil dihapus');
    }
    public function home(){
        $jumlah_anggota = DB::select("call jumlahanggota()");
        $jumlah_buku = DB::select("CALL JumlahBuku()");
        $jumlah_peminjaman = DB::select("call jumlahpeminjaman()");
        return view('home',['jumlah_buku' => $jumlah_buku,'jumlah_peminjaman' => $jumlah_peminjaman,'jumlah_anggota' => $jumlah_anggota]);
    }

}
